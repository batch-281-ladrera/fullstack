const txtFirstName = document.querySelector("#txt-first-name");
const spanFullName = document.querySelector("#span-full-name");
const txtLastName = document.querySelector("#txt-last-name");

let firstName = "";
let lastName = "";

function updateFirstName(e) {
  firstName = e.target.value;
  spanFullName.innerHTML = `${firstName} ${lastName}`;
}

function updateLastName(e) {
  lastName = e.target.value;
  spanFullName.innerHTML = `${firstName} ${lastName}`;
}

txtFirstName.addEventListener("keyup", updateFirstName);
txtLastName.addEventListener("keyup", updateLastName);
