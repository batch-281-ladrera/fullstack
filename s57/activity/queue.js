let collection = [];

// Write the queue functions below.

function print() {
  // It will show the array
  return collection;
}

function enqueue(element) {
  //In this function you are going to make an algo that will add an element to the array (last element)
  collection[collection.length] = element;
  return collection;
}

function dequeue() {
  // In here you are going to remove the first element in the array
  for (let i = 1; i < collection.length; i++) {
    collection[i - 1] = collection[i];
    collection.length = collection.length - 1;
    return collection;
  }
}

function front() {
  // you will get the first element
  return collection[0];
}

function size() {
  // Number of elements
  let count = 1;
  while (collection[count - 1] != undefined) {
    count = count + 1;
  }
  return count - 1;
}

function isEmpty() {
  //it will check whether the function is empty or not
  if (collection[0] === undefined) {
    return true;
  } else {
    return false;
  }
}

module.exports = {
  collection,
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
};
